package RestAssured;

import org.testng.annotations.Test;

import static RestAssured.ClientInfo.*;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.matchesPattern;

public class TestRestAssured extends BaseTest{

    @Test
    public void buyBitCoinTest(){

        String payload = "{\n" +
                "    \"cash\": false,\n" +
                "    \"city\": \"msk\",\n" +
                "    \"client_email\": \""+email+"\",\n" +
                "    \"client_phone\": \"\",\n" +
                "    \"tg\": \"\",\n" +
                "    \"language\": \"RU\",\n" +
                "    \"from_exchange\": \"SBERRUB\",\n" +
                "    \"to_exchange\": \"BTC\",\n" +
                "    \"cards_number\": \""+card+"\",\n" +
                "    \"receiver_address\": \""+wallet+"\",\n" +
                "    \"ref_id\": 0,\n" +
                "    \"sum_get\": 1000,\n" +
                "    \"sum_give\": 0.002344554659768144,\n" +
                "    \"lock_get\": false,\n" +
                "    \"dest_tag\": \"\",\n" +
                "    \"client_name\": \"\"\n" +
                "}";
        given().
                body(payload).
                when().
                post("/create").
                then().
                log().all().
                assertThat().
                body("success", equalTo(true),
                        "content.hash", matchesPattern("[a-z0-9-]{65}$"));
    }
}
