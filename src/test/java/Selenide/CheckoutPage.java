package Selenide;

import com.codeborne.selenide.Condition;
import org.openqa.selenium.By;

import java.time.Duration;

import static com.codeborne.selenide.Selenide.$;

public class CheckoutPage extends BaseTest {

    private final By qrCode = By.xpath("//div[@class='PurchaseRequest_qrContainerdown__telMG'] ");

    public CheckoutPage checkByQrCode() {

        $(qrCode).shouldBe(Condition.visible, Duration.ofSeconds(20));
        return this;
    }
}
