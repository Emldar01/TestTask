package Selenide;

import Selenide.BasePage;
import Selenide.CheckoutPage;

public class TestSelenide extends BasePage {

    @org.testng.annotations.Test
    public void buyBitCoinTest() throws InterruptedException {
        BasePage basePage = new BasePage();
        basePage.openBasePage().
                setEmailValue("TestEmail@gmail.com").
                setCardNumberValue("5106211010255078").
                setCryptoWalletValue("1BvBMSEYstWetqTFn5Au4m4GFg7xJaNVN2").
                plusDigitToMinAmount("0").
                clickBuyBtn();

        CheckoutPage checkoutPage = new CheckoutPage();
        checkoutPage.checkByQrCode();
    }

}
