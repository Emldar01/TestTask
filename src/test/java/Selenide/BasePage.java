package Selenide;

import com.codeborne.selenide.Condition;
import org.openqa.selenium.By;

import java.time.Duration;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class BasePage extends BaseTest {

    private final String basePageUrl = "https://bit-changer.cc/";
    private final By emailFld = By.xpath("//input[@id='currencyEmailInput']");
    private final By cardNumberFld = By.xpath("//input[@id='currencyCardNumberInput']");
    private final By cryptoWalletFld = By.xpath("//input[@id='currencyCryptoWalletInput']");
    private final By buyBtn = By.xpath("(//button[@class='undefined Button_button__JBBzO'])[2]");
    private final By amountToGiveFld = By.xpath("(//input[@id='firstInput'])[1]");
    private final By minAmountToGive = By.xpath("//span[@class='Currency_textActive__cW0iV']");

    public BasePage openBasePage() {
        open(basePageUrl);
        return this;
    }
    public BasePage setEmailValue(String email) {
        $(emailFld).setValue(email);
        return this;
    }
    public BasePage setCardNumberValue(String cardNumber) {
        $(cardNumberFld).setValue(cardNumber);
        return this;
    }

    public BasePage setCryptoWalletValue(String walletNumber) {
        $(cryptoWalletFld).setValue(walletNumber);
        return this;
    }

    public BasePage plusDigitToMinAmount(String amount) throws InterruptedException {
        Thread.sleep(1000);
        clickBtn(minAmountToGive);
        $(amountToGiveFld).setValue(amount);
        return this;
    }

    public BasePage clickBuyBtn() {
        clickBtn(buyBtn);
        $(By.xpath("//div[@class='Spinner_spinner__H2HDD']")).shouldBe(Condition.disappear, Duration.ofSeconds(20));
        return this;
    }

}
