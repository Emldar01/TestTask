package Selenide;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.time.Duration;

import static com.codeborne.selenide.Selenide.$;

public class BaseTest {


    @BeforeMethod
    protected void startDriver() {
        Configuration.holdBrowserOpen = true;
        Configuration.screenshots = false;
        Configuration.savePageSource = false;
        Configuration.browserSize = "1920x1080";
        WebDriverManager.chromedriver().cachePath("chromeWebDriver").setup();
    }

    protected void clickBtn(By element) {
        $(element).shouldBe(Condition.enabled, Duration.ofSeconds(20)).click();
    }

    @AfterMethod
    protected void closeDriver() {
        Selenide.clearBrowserCookies();
        Selenide.clearBrowserLocalStorage();
        Selenide.closeWebDriver();
        WebDriverManager.chromedriver().cachePath("chromeWebDriver").quit();
    }
}
